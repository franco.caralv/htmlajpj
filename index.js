const express = require('express')
const cors = require("cors");
const app = express()
const nodeHtmlToImage = require('node-html-to-image')
const fs = require('fs');
const image = fs.readFileSync('./MARCO UR-03.png');
const base64Image = new Buffer.from(image).toString('base64');
app.use(cors());
app.use(express.json({limit: '50mb'}));
app.use(express.urlencoded({limit: '50mb'}));
const dataURI = 'data:image/jpeg;base64,' + base64Image;
app.post('/', async function (req, res) {
    const html = `<html>
    <head>
      <style>
      .eye{
        position:absolute;
        height:200px;
        width:200px;
        top: 40px;
        left : 40px;
        z-index: 1;
      }
      
      .heaven
      {
        position:absolute;
        height:300px;
        width:300px;
        display: flex;
          align-items: center;
          justify-content: center;
        z-index: -1;
      }
      .posicionimg{
      
          width: 60%;
          height: 60%;
          margin-right: 35px;
          margin-bottom: 35px;
      }
      </style>
    </head>
    <body>
    <div style="position: relative; left: 0; top: 0;">
		<img src={{imagendeentrada}} class='eye'/>
        <div class="heaven" >
            <img width="100px" height="100px" class="posicionimg" src={{imagemotra}} />
        </div>
	</div>
    </body>
  </html>
  `;
    let data = req.body;

    const uriimagen = data.foto;
    const image = await nodeHtmlToImage({
        html: html,
        content: {
            imagendeentrada: dataURI,
            imagemotra: uriimagen
        }
    });
    res.writeHead(200, { 'Content-Type': 'image/png' });
    res.end(image, 'binary');
})

app.listen(3000)
