import React, {useState} from "react";
import {Crop} from "react-image-crop";
import {centerAspectCrop} from "../App";


export const useImagenSelect=()=>{

    const [imgSrc, setImgSrc] = useState('')
    const [crop, setCrop] = useState<Crop>()
    const [pantalla, setpantalla] = useState<Boolean>(true)
    const [aspect, setAspect] = useState<number | undefined>(1)

    function onSelectFile(e: any) {
        if (e.files && e.files.length > 0) {
            setCrop(undefined) // Makes crop preview update between images.
            const reader = new FileReader()
            reader.addEventListener('load', () => {

                    //@ts-ignore
                    setImgSrc(reader.result.toString() || '');
                }
            )
            setpantalla(false);
            reader.readAsDataURL(e.files[0])
        }
    }

    function onImageLoad(e: React.SyntheticEvent<HTMLImageElement>) {
        if (aspect) {
            const {width, height} = e.currentTarget
            setCrop(centerAspectCrop(width, height, aspect))
        }
    }


    function  regresarpantalla(){
        setpantalla(true);
    }


    return {
        imgSrc,
        setImgSrc,
        setCrop,
        crop,
        setpantalla,
        pantalla,
        regresarpantalla,
        onSelectFile,
        onImageLoad,
        setAspect,
        aspect
    }


}