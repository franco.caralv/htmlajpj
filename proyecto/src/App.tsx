import React, {useCallback, useRef, useState} from 'react'

import ReactCrop, {centerCrop, Crop, makeAspectCrop, PixelCrop,} from 'react-image-crop'
import {canvasPreview} from './canvasPreview'
import {useDebounceEffect} from './useDebounceEffect'

import 'react-image-crop/dist/ReactCrop.css'
import {toPng} from "html-to-image";
import "./App.css"
import {ProgressBar} from 'primereact/progressbar';

import imagen from './imgenes/marco1.png';

import "primereact/resources/themes/lara-light-indigo/theme.css"; //theme
import "primereact/resources/primereact.min.css"; //core css
import "primeicons/primeicons.css";
import {Button} from 'primereact/button';
import {Pantallaone} from "./components/pantallaone";
import {Pantallatwo} from "./components/pantallatwo";
import {useImagenSelect} from "./hooks/useImagenSelect";
// This is to demonstate how to make and center a % aspect crop
// which is a bit trickier so we use some helper functions.
export function centerAspectCrop(
    mediaWidth: number,
    mediaHeight: number,
    aspect: number,
) {
    return centerCrop(
        makeAspectCrop(
            {
                unit: '%',
                width: 90,
            },
            aspect,
            mediaWidth,
            mediaHeight,
        ),
        mediaWidth,
        mediaHeight,
    )
}


export default function App() {


    const previewCanvasRef = useRef<HTMLCanvasElement>(null)
    const imgRef = useRef<HTMLImageElement>(null)

    const [completedCrop, setCompletedCrop] = useState<PixelCrop>()
    const [scale, setScale] = useState(1)
    const [rotate, setRotate] = useState(0)


    const {onSelectFile,pantalla,onImageLoad,setCrop,imgSrc,regresarpantalla,crop,aspect}=useImagenSelect()



    useDebounceEffect(
        async () => {
            if (
                completedCrop?.width &&
                completedCrop?.height &&
                imgRef.current &&
                previewCanvasRef.current
            ) {
                // We use canvasPreview as it's much faster than imgPreview.
                canvasPreview(
                    imgRef.current,
                    previewCanvasRef.current,
                    completedCrop,
                    scale,
                    rotate,
                )
            }
        },
        100,
        [completedCrop, scale, rotate],
    )

   /* function handleToggleAspectClick() {
        if (aspect) {
            setAspect(undefined)
        } else if (imgRef.current) {
            const {width, height} = imgRef.current
            setAspect(16 / 9)
            setCrop(centerAspectCrop(width, height, 16 / 9))
        }
    }*/



    if (pantalla) {
        return (
            <Pantallaone onSelectFile={onSelectFile} ></Pantallaone>
        );
    } else {
        return (
            <Pantallatwo completedCrop={completedCrop} regresarpantalla={regresarpantalla} previewCanvasRef={previewCanvasRef} imgSrc={imgSrc} aspect={aspect} setCompletedCrop={setCompletedCrop} crop={crop}
            onImageLoad={onImageLoad} setCrop={setCrop} imgRef={imgRef} rotate={rotate} scale={scale}  ></Pantallatwo>
        )
    }


}

