import logo from "../imgenes/LOGO (1).png";
import {ProgressBar} from "primereact/progressbar";
import {FileUpload} from "primereact/fileupload";
import elemetos from "../imgenes/elementos.png";
import React from "react";


 export const Pantallaone = ({onSelectFile}:any): any => {


    return <>
        <div className="App formato">
            <div
            className="flex w-full justify-center mb-1 "
            >
                <img id='unidaddedo'
                     className="w-max sm:h-[200px] md:h-[200px] "
                     src={logo} width={100} height={200} alt={"Logo"}/>
            </div>
            <p style={{fontWeight: "bold"}}>
                Seleciona tu fotografia (Paso 1/2)
            </p>
            <ProgressBar value={50}/>
            <div className="Crop-Controls">
                <FileUpload chooseLabel="Elegir Foto" name="demo" accept="image/*" customUpload
                            uploadHandler={onSelectFile} auto/>
            </div>
            <div style={{
                width: '100%',
                display: 'flex',
                justifyContent: 'center',
                marginBottom: '10px',
            }}>
                <img src={elemetos} width={100} height={30}
                     className="w-max h-[30px]" alt={"elementos"}/>
            </div>
            <div style={{
                width: '100%',
                display: 'flex',
                justifyContent: 'center',
                marginBottom: '10px',
            }}>
                <a  href={'https://wa.me/51921156475?text=Hola%20franco%20'}> <h5 style={{
                    color: "#0071C2"
                }}>Conctato Aqui</h5>
                </a>
            </div>
        </div>
    </>

}
