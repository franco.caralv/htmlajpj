import {Button} from "primereact/button";
import {ProgressBar} from "primereact/progressbar";
import imagen from "../imgenes/marco1.png";
import imgene2 from "../imgenes/marco2.png"
import imgene3 from "../imgenes/imagen.png"
import ReactCrop from "react-image-crop";
import React, {useCallback, useRef, useState} from "react";
import {toPng} from "html-to-image";


export const Pantallatwo = ({completedCrop,regresarpantalla,previewCanvasRef,imgSrc,aspect,setCompletedCrop,crop,
                                onImageLoad,setCrop,imgRef,rotate,scale
}:any)=>{
    const [urlimagen,seturlimagen] = useState(imagen)

    function  cambioimagen(img:string){
        seturlimagen(img);
    }
    const ref = useRef<HTMLDivElement>(null)

    const cropimagen = useRef<HTMLDivElement>(null)
    const onButtonClick = useCallback(() => {
        if (ref.current === null) {
            return
        }

        toPng(ref.current, {cacheBust: true, quality: 1})
            .then((dataUrl) => {
                console.log(dataUrl);
                const link = document.createElement('a')
                link.download = 'my-image-name.png'
                link.href = dataUrl
                document.body.appendChild(link);
                link.click()
                document.body.removeChild(link);
            })
            .catch((err) => {
                console.error(err)
            })
    }, [ref])

const cropimagenfuncion=()=>{
    if(cropimagen?.current)
    {

        window.scroll({
            top: cropimagen.current.scrollTop+700,
            behavior: 'smooth'
        });
    }
}


    return <>
        <div className="App">
            <div className="flex justify-center w-full">
                <Button className="mb-1.5" label="Regresar a Escoger Foto" aria-label="Submit" onClick={regresarpantalla}  />
            </div>

            <p style={{ fontWeight: "bold"}}>
                Seleciona tu fotografia (Paso 2/2)
            </p>

            <ProgressBar value={100} style={{
                margin:"20px 0px"
            }}/>
            <div className="flex justify-center w-full mb-1">
                <Button className="mb-1.5" label="Modificar Foto" aria-label="Submit" onClick={cropimagenfuncion}  />
            </div>
            <div className="flex mb-2 space-x-2 w-full flex-row items-center justify-center">
                <Button className="bg-white background-tranparent"
                        onClick={
                            ()=>{
                                cambioimagen(imagen)
                            }
                        }>
                    <img src={imagen} className="w-max h-10" />
                </Button>
                <Button className="bg-white background-tranparent"
                        onClick={
                            ()=> cambioimagen(imgene2)
                        }>
                    <img src={imgene2} className="w-max h-10"/>
                </Button>
                <Button className="bg-white background-tranparent"
                        onClick={
                            ()=>cambioimagen(imgene3)
                        }>
                    <img src={imgene3} className="w-max h-10" />
                </Button>
            </div>
            <div className="flex justify-center" >
                {Boolean(completedCrop) && (
                    <div style={{
                        width: "fit-content"
                    }}>
                        <div  id="ref" ref={ref}  className={"jodido"}>
                            <img  src={urlimagen} className={'eye'} alt={"asda"}/>
                            <div className={"heaven"}>
                                <canvas
                                    ref={previewCanvasRef}
                                    className={"posicionimg"}
                                />
                            </div>
                        </div>
                    </div>

                )}
            </div>
            {Boolean(completedCrop) && (
                <Button style={{
                    margin:"10px",
                    width: "100%"
                }} label="Descargar" onClick={onButtonClick}/>

            )}
            <p  style={{
                color: "#0071C2",
                fontWeight: "bold"
            }}>Modifica la foto a tu gusto</p>
            {Boolean(imgSrc) && (
                <div ref={cropimagen} className="flex justify-center">
                    <ReactCrop
                        crop={crop}
                        onChange={(_, percentCrop) => setCrop(percentCrop)}
                        onComplete={(c) => setCompletedCrop(c)}
                        aspect={aspect}
                    >
                        <img
                            ref={imgRef}
                            width={50}
                            height={50}
                            alt="Crop me"
                            src={imgSrc}
                            style={{transform: `scale(${scale}) rotate(${rotate}deg)`, width: "auto", height: "400px"}}
                            onLoad={onImageLoad}
                        />
                    </ReactCrop>
                </div>
            )}
            <div
            className="w-full flex mb-3 justify-center"
            >
                <a  href={'https://wa.me/51921156475?text=Hola%20franco%20'}> <h5 style={{
                    color: "#0071C2"
                }}>Conctato Aqui</h5>
                </a>
            </div>

        </div>
    </>
}