/** @type {import('tailwindcss').Config} */
const colors = require('tailwindcss/colors');

module.exports = {
  important: true,
  separator: '_',
  mode: 'jit',
  darkMode: false,
  purge: {
    enabled: true,
    content: ["./src/**/*.{html,js,ts,tsx}"]
  },
  theme: {
    extend: {
      screens: {
        'tablet': '640px',
        'laptop': '1024px',
        'desktop': '1280px',
        'sm': { 'min': '640px', 'max': '767px' },
        'xl': { 'min': '1280px', 'max': '1500px' },
        'lg': { 'min': '1024px', 'max': '1279px' },
        'md': { 'min': '768px', 'max': '1023px' },
        '1xl': '1500px',
        'mil': '1224px',
        'millim': { 'min': '8px', 'max': '1224px' },
        'mdv2': '768px'
      },
      colors: {
        'primary': '#6DC560',
        'blues': '#F5F7FC',
        'body-base': '#636E95',
        'body-dark': '#454E5A',
        'control': '#e1e3e5',
        'link': colors.blue[500],
        'separator': colors.gray[200],
      }
    }
  },
  plugins: [],
}
